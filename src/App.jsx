import { useState, useEffect ,createContext} from "react";
import Loader from "./Components/Loader";
import Showcart from "./Components/Showcart";
import Navbar from "./Components/Navbar";
import Searchbar from "./Components/Searchbar";
import Showcountrydata from "./Components/Showcountrydata";
import Errorpage from './Components/Errorpage';
import { Route, Routes } from "react-router-dom";

export const ThemeContext = createContext();
function App() {

  const [isDarkMode, setDarkMode] = useState(false);
 

  const toggleMode = () => {
    setDarkMode(!isDarkMode);
  };

  const [country, setCountry] = useState([]);
  const [loading, setloading] = useState(true);
  const [currRegion, setCurrRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [searchName, setSearchName] = useState("");
  const [sortingData, setSortingData] = useState("");

  useEffect(() => {
    let url = "https://restcountries.com/v3.1/all";
    fetch(url)
      .then((data) => {
        setloading(false);
        return data.json();
      })
      .then((item) => {
        setCountry(item);
      });
  }, []);

  // finding region

  const CountryRegion = country.reduce((acc, data) => {
    if (!acc[data.region]) {
      acc[data.region] = data.region;
    }
    return acc;
  }, []);

// filter country by region

  let regionData = country.filter((item) => {
    return item.region.includes(currRegion);
  });

  // finding subregion

  const subregiondata = regionData.reduce((acc, data) => {
    if (!acc[data.subregion]) acc[data.subregion] = data.subregion;

    return acc;
  }, {});


  // finding country by subregion

  const subregionfind = regionData.filter((item) => {
    return item.subregion == subRegion;
  });
  if (subregionfind.length > 0) {
    regionData = subregionfind;
  }
// finding country by name on search

  regionData = regionData.filter((item) => {
    return item.name.common.toLowerCase().includes(searchName.toLowerCase());
  });

 

  // sorting data on the basis on population and area

  if (sortingData == "decreasePopulation") {
    regionData.sort((curr, prev) => {
      return curr.population - prev.population;
    });
  } else if (sortingData == "increasePopulation") {
    regionData.sort((curr, prev) => {
      return prev.population - curr.population;
    });
  } else if (sortingData == "decreaseArea") {
    regionData.sort((curr, prev) => {
      return curr.area - prev.area;
    });
  } else if (sortingData == "increaseArea") {
    regionData.sort((curr, prev) => {
      return prev.area - curr.area;
    });
  }

  return (
    <>
      <ThemeContext.Provider value={isDarkMode}>
     <Navbar changevalue={toggleMode}/>
      <Routes>
        <Route
          path="/"
          element={
            <>
              <Searchbar
                region={CountryRegion}
                subregion={subregiondata}
                selectRegion={setCurrRegion}
                selectSubRegion={setSubRegion}
                searchData={setSearchName}
                sortingData={setSortingData}
              />
              {loading ? <Loader /> : <Showcart item={regionData} />}
             {regionData.length == 0 && searchName.length > 0 ? <Errorpage /> : ""}

            </>
          }
        ></Route>
        <Route
          path={"/country/:id"}
          element={ regionData.length==0?<Loader/>:<Showcountrydata country={country} />}
        />
      </Routes>
      </ThemeContext.Provider>
    </>
  );
}

export default App;
