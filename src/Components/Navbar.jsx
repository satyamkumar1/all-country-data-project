import { useContext } from "react";
import "./Style.css";
import { ThemeContext } from "../App";

const Navbar = ({changevalue}) => {
  const isDarkMode = useContext(ThemeContext);
  function handlecolor() {
    changevalue();

    if(isDarkMode){
      document.body.className="whitemode";
    }
    else{
      document.body.className="darkmode";

    }

  }
  return (
    <>
      <div className={` header ${isDarkMode?"dark-navbar":"header"}`}>
        <h1>Where in the world?</h1>

        <button className="btn-mode" onClick={handlecolor}>
          <i className="fa-solid fa-moon"></i>
         {isDarkMode? "white-mode":"dark-mode"}
        </button>
      </div>
    </>
  );
};

export default Navbar;
