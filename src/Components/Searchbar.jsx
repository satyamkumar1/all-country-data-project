import "./Style.css";
import { ThemeContext } from "../App";
import { useContext } from "react";

const Serachbar = ({ region, subregion, selectRegion,selectSubRegion ,searchData,sortingData}) => {
  const isDarkMode = useContext(ThemeContext);
 
  return (
    <>
      <div className="head-section">
        <div className={`header-section ${isDarkMode? 'search-dark':
      'header-section'}`}>
          <input
            className="input-search"
            type="text"
            placeholder="serach for a country"
            onChange={(e) => searchData(e.target.value)}
          />

          <select onChange={(e) => selectRegion(e.target.value)}>
            <option value={"all"}> Filter by region</option>
            {Object.values(region).map((data) => {
              return <option key={data}>{data}</option>;
            })}
          </select>

          {
            <select onChange={(e) => selectSubRegion(e.target.value)}>
              <option> Filter by subregion</option>
              {Object.values(subregion).map((data) => {
                return <option key={data+"1"}>{data}</option>;
              })}
            </select>
          }

          <select onChange={(e) => sortingData(e.target.value)}>
            <option> sort by</option>
            <option value={"decreasePopulation"}>decrease by population </option>
            <option value={"increasePopulation"}>increase by population </option>
            <option value={"decreseArea"}>decrease by area </option>
            <option value={"increaseArea"}>increase by area </option>
          </select>
        </div>
      </div>
    </>
  );
};

export default Serachbar;
