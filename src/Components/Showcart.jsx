import { useContext} from "react";
import "./Style.css";
import { Link } from "react-router-dom";
import { ThemeContext } from "../App";
const Showcart = ({ item }) => {
   
  const isDarkMode = useContext(ThemeContext);
  return (
    <>
      <div className={`main-box ${isDarkMode ? 'cart-area' :"main-box"}`}>
        {item.map((item) => {
          return (
            <Link to={`/country/${item.cca3}`}  key={item.cca3} className="cart-link" > 
            <div  className={"cart"} >
             <img className="photo" src={item.flags.png} />
              <div className="content-data">
                <h1>{item.name.common}</h1>
                <p>Population:{item.population}</p>
                <p>Region:{item.region}</p>
                <p>Capital:{item.capital}</p>
              </div>
            </div></Link>
          );
        })}
      </div>
    </>
  );
};

export default Showcart;
